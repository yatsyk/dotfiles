runtime config/before_plugins.vimrc
runtime config/load_plugins.vimrc
runtime config/menu.vimrc
runtime config/filetype.vimrc
runtime config/spellcheck.vimrc
runtime config/colors.vimrc
runtime config/plugin_settings.vimrc
runtime config/folding.vimrc
runtime config/hexeditor.vimrc
runtime config/journal.vimrc



if has("linebreak")
  let &sbr = nr2char(8618).' '
endif

" use :w!! to write to a file using sudo if you forgot to 'sudo vim file'
" (it will prompt for sudo password when writing)
cmap w!! w !sudo tee % >/dev/null

" Opens file under cursor in new split
nmap gf :vertical wincmd f<CR>

" Reselect after indenting
vnoremap < <gv
vnoremap > >gv


set keymap=russian-jcuken
" set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

set relativenumber

syntax enable
set modeline           " Modelines (comments that set vim options on a per-file basis)
set nohidden           " Do not let to change buffers without saving
set autoread           " Automatically reload changes if detected
set history=1000       " Number of things to remember in history
set undolevels=700     " More undo levels
set confirm            " Confirmation instead of fails on e.g. :q
" set clipboard+=unnamed " Yanks go on clipboard instead
set autowrite          " Writes on make/shell commands
set timeoutlen=500     " Time to wait for a command
set foldlevelstart=99  " No folds closed on start
set showmode           " Show mode at the bottom
set infercase          " Completion recognizes capitalization
set shortmess+=A       " Don't bother me when a swapfile exists
set ttyfast
set diffopt+=iwhite

" Searching
set ignorecase  " Case insensitive search
set smartcase   " Non-case sensitive search if contains upper
set incsearch   " While typing a search command, show show where the pattern
set hlsearch    " Enable search highlighting
nohlsearch      " but do not highlight last search on startup
set wildignore+=*.o,*.obj,*.exe,*.so,*.dll,*.pyc,.svn,.hg,.bzr,.git,.sass-cache,*.class
set wildignore+=*.DS_Store


" Disable Sounds
set noerrorbells
set novisualbell
set t_vb=

" Mouse
set mousehide   " Hide mouse after chars typed
set mouse=a     " Mouse in all mode
set mouseshape=s:udsizing,m:no " turn to a sizing arrow over the status liness

" Complete options
set complete+=U


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" User Interface
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set linespace=1    " in pixels
if has('gui_macvim')
  set guifont=Hack\ Regular\ Nerd\ Font\ Complete:h18
  " set linespace=-3 " fix Inconsolata LGC
  set macmeta      " Use option (alt) as meta key.
elseif has("gui_gtk2")
  set guifont=Inconsolata\ LGC\ 17
endif

if has("gui_running")
  set guioptions=ai
endif

" set window title to 'titlestring' or to: filename [+=-] (path) - VIM
set title

" set anti                " Antialias font
set ruler               " Show row/col and percentage
set number              " Line numbers on
set nowrap              " Line wrapping off
set laststatus=2        " Always show the statusline
set cmdheight=1
set signcolumn=yes


" Disable balloon hints on mouseover
" set noballooneval
let g:netrw_nobeval = 1
let g:ale_set_balloons = 0

" Commands autocomplete options
set wildmode=list:longest,full
set wildmenu        " Turn on WiLd menu

set showmatch       " Show matching brackets.
set matchtime=2     " How many tenths of a second to blink
set showcmd         " Show command in the last line
set showtabline=2   " Show tabline only if there are at least two tab pages
set cursorline      " color current line
set gcr=n:blinkon0  " do not blink cursor in normal mode

set colorcolumn=120
set undofile

" new splits below and right
set splitbelow
set splitright




""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Text Formatting
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set backspace=indent,eol,start " make backspace work like most other apps
set cindent " Automatic program indenting
set autoindent " Carry over indenting from previous line
set smarttab
set wrap            " http://vimcasts.org/episodes/soft-wrapping-text/
set linebreak       " Break long lines by word, not char
set list            " display unprintable characters
set listchars=tab:▸\ ,extends:›,precedes:‹,nbsp:×,trail:· " Unicode characters for various things
set formatoptions=crqln1   " :h fo-table

" Show ↪ at the beginning of wrapped lines
if has("linebreak")
  let &sbr = nr2char(8618).' '
endif




""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Bindings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set leader to ,
" Note: This line MUST come before any <leader> mappings
let mapleader=","

" Fixes common typos
command! W w
command! Q q
map <F1> <Esc>
imap <F1> <Esc>
map Q  <silent>
map q: <silent>

" Reselect after indenting
vnoremap < <gv
vnoremap > >gv

" Search result always at the center
nmap n nzz
nmap N Nzz
nmap * *zz
nmap # #zz
nmap g* g*zz
nmap g# g#zz

" Don't skip wrap lines
noremap j gj
noremap k gk

" key `Tab` -- switch between current/prev buffers
nnoremap <Tab> <C-^>

" key `K` -- search word under cursor with Ag
" nnoremap K :silent! Ack! -i "\b<cword>\b"<cr>

" key `<C-{k,j}>` -- move lines up/down
nmap <C-k> [e
nmap <C-j> ]e
vmap <C-k> [egv
vmap <C-j> ]egv

" use :w!! to write to a file using sudo if you forgot to 'sudo vim file'
" (it will prompt for sudo password when writing)
" cmd `<:w!!>` -- write as sudo
cmap w!! w !sudo tee % >/dev/null

" double percentage sign in command mode is expanded
" to directory of current file
" cmd `<%%>` -- current file directory
cnoremap %% <C-R>=expand('%:p').'/'<cr>

" show generated vimrc doc
command! Mydoc :!$HOME/.vim/gen_doc.sh \|more

" key `<,<space>>` -- clean search hl and save the buffer
nmap <leader><space> :nohlsearch<cr>
" key `<Enter>` -- clean search hl and save the buffer
nmap <cr> :nohlsearch<cr>:w<cr>

" key `<,sp>` -- toggle spelling mode
nmap <leader>sp :set spell! spelllang=ru,en spell?<CR>

" key `<,w>` -- toggle wrapping
nmap <silent> <leader>w :set invwrap wrap?<CR>

" key `<,hs>` -- toggle hlsearch with
nmap <leader>hs :set hlsearch! hlsearch?<CR>

" key `<,p>` -- toggle paste mode
nmap <leader>p :set paste! paste?<cr>

" key `<,s>` -- :%s##
nnoremap <leader>s :%s##<left>

" key `<,W>` -- fix trailing white space
map <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

" key `<,mk>` -- create the directory containing the file in the buffer
nmap <silent> <leader>mk :!mkdir -p %:p:h<CR>

" key `<,ft>` -- format markdown table
map <leader>ft V{jo}k:s/----*/---/g<CR>gv:Tabularize /\|<CR>j:s/\(\s*\)\(:\)\?\(-*\)\(:\)\?\(\s*\)/\2\1\3\5\4/g<CR>:s/\s/-/g<CR>gv:s/---$/-----------/<CR>:noh<CR>

" ctags
" TODO: check out https://github.com/junegunn/fzf/issues/243
" ket `<t>` -- next tag match
nmap t <C-]>
" ket `<T>` -- previous tag match
nmap T :pop<CR>

"""" Experimental

" key `<U=>` -- underline the current line with '='
nmap <silent> U= YpVr=<cr>
" key `<U">` -- underline the current line with '"'
nmap <silent> U" YpVr"<cr>
" key `<Ur>` -- update underline
nmap <silent> Ur jvyddkYpVr"<cr>

" key `<,ev>` -- edit my vimrc
nmap <leader>ev :tabedit $MYVIMRC<CR>
" key `<,rv>` -- reload vimrc
nmap <leader>rv :so $MYVIMRC<CR>

" key `<,fef>` -- reformat the entire file
" nmap <leader>fef mQggVG=`Q

" key `<+>` -- fold code till matched bracket
map + v%zf

" key `<,v>` -- reselect pasted text
nnoremap <leader>v V`]

"""" Tabs/buffers navigation

" key `<c-#>` -- switches to tab
nmap <d-s-1> 1gt
nmap <d-s-2> 2gt
nmap <d-s-3> 3gt
nmap <d-s-4> 4gt
nmap <d-s-5> 5gt
nmap <d-s-6> 6gt
nmap <d-s-7> 7gt
nmap <d-s-8> 8gt
nmap <d-s-9> 9gt
nmap <d-1> 1gt
nmap <d-2> 2gt
nmap <d-3> 3gt
nmap <d-4> 4gt
nmap <d-5> 5gt
nmap <d-6> 6gt
nmap <d-7> 7gt
nmap <d-8> 8gt
nmap <d-9> 9gt



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Auto Commands
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if has("autocmd")
  " Autosave all buffers
  autocmd FocusLost silent! :wa

  " Resize splits when the window is resized
  autocmd VimResized * wincmd =

  " Remember last location in file, but not for commit messages.
  " see :help last-position-jump
  autocmd BufReadPost *
        \ if &filetype !~ '^git\c' && line("'\"") > 0 && line("'\"") <= line("$") |
        \   exe "normal! g`\"" |
        \ endif
endif


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""



set timeoutlen=1000 ttimeoutlen=0
