call plug#begin('~/.local/share/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'dense-analysis/ale' " to configure
Plug 'liuchengxu/vista.vim'

Plug 'bronson/vim-trailing-whitespace'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'farmergreg/vim-lastplace'
Plug 'haya14busa/incsearch.vim'
Plug 'haya14busa/incsearch-fuzzy.vim'
Plug 'rizzatti/dash.vim'
Plug 'ap/vim-css-color'
Plug 'sjl/gundo.vim'
Plug 'tpope/vim-projectionist'
Plug 'mileszs/ack.vim'
" Plug 'Shougo/deoplete.nvim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'vim-airline/vim-airline'
Plug 'easymotion/vim-easymotion'
Plug 'sh-dude/ZoomWin'
Plug 'garbas/vim-snipmate'

Plug 'scrooloose/nerdcommenter'
" Plug 'terryma/vim-multiple-cursors'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'tomtom/tlib_vim'
Plug 'metalelf0/supertab'
" FIX Plug 'ervandew/supertab'
Plug 'amix/vim-zenroom2'
Plug 'junegunn/goyo.vim'
Plug 'yegappan/mru'
Plug 'prettier/vim-prettier'
Plug 'jamessan/vim-gnupg'

" Colors http://vimcolors.com/
Plug 'severij/vadelma'
Plug 'matveyt/vim-modest'
Plug 'kyledoherty/espresso-colors-vim'
Plug 't184256/vim-boring'
Plug 'RussellBradley/vim-nba'
Plug 'sainnhe/vim-color-ice-age'
Plug 'tacahiroy/vim-colors-isotake'
Plug 'chriskempson/base16'
Plug 'jaywilliams/vim-vwilight'
Plug 'altercation/vim-colors-solarized'
Plug 'larssmit/vim-getafe'
Plug 'morhetz/gruvbox'
Plug 'w0ng/vim-hybrid'
Plug 'cocopon/iceberg.vim'
Plug 'kristijanhusak/vim-hybrid-material'
Plug 'vim-airline/vim-airline-themes'
Plug 'dracula/vim'
Plug 'sickill/vim-monokai'
Plug 'mightwork/summerfruit256.vim'
Plug 'levelone/tequila-sunrise.vim'

" Languages
" Plug 'sheerun/vim-polyglot'
" Plug 'vim-ruby/vim-ruby'
" Plug 'tpope/vim-rails'
" Plug 'tpope/vim-rake'
" Plug 'tpope/vim-cucumber'
" Plug 'janko-m/vim-test'
" Plug 'pearofducks/ansible-vim'
" Plug 'plasticboy/vim-markdown'
" Plug 'pangloss/vim-javascript'
" Plug 'fatih/vim-go'
" Plug 'udalov/kotlin-vim'
" Plug 'jdonaldson/vaxe'
" Plug 'mdempsky/gocode', {'rtp': 'vim/'}
"Plug 'ekalinin/Dockerfile.vim'
" Plug 'mxw/vim-jsx'
" Plug 'mmalecki/vim-node.js'


" Track the engine.
" Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
" Plug 'honza/vim-snippets'

" last plugin
Plug 'ryanoasis/vim-devicons'

call plug#end()

