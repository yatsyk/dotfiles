runtime config/plugin_settings/nerdtree.vimrc

""" Prettier

" let g:prettier#config#parser = 'babylon'

""" Gpg

let g:GPGPreferArmor=1
let g:GPGExecutable = "PINENTRY_USER_DATA='' gpg --trust-model always"
let g:GPGDefaultRecipients=["yatsyk@gmail.com"]

nnoremap <silent> <leader>z :Goyo<cr>

" show recenly opened files
map <leader>f :MRU<CR>

""" ALE
let g:ale_sign_warning = '▲'
let g:ale_sign_error = '✗'

""" Deoplete
if has('python3')
  let g:deoplete#enable_at_startup = 1
  let g:deoplete#disable_auto_complete = 1
  " let g:deoplete#enable_auto_select = 1
  let g:deoplete#enable_smart_case = 1
  let g:deoplete#enable_yarp = 1

  " let g:deoplete#sources#go#gocode_binary = $GOPATH.'/bin/gocode'
  " let g:deoplete#sources#go#sort_class = ['package', 'func', 'type', 'var', 'const']

  autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

  inoremap <silent><expr><tab>
        \ pumvisible() ? "\<c-n>" :
        \ <sid>check_back_space() ? "\<tab>" :
        \ deoplete#mappings#manual_complete().deoplete#mappings#manual_complete()
  inoremap <silent><expr><s-tab> pumvisible() ? "\<c-p>" : "\<s-tab>"
  inoremap <silent><expr><bs> pumvisible() ? deoplete#smart_close_popup() : "\<bs>"

  function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
  endfunction
endif

""" Airline
let g:airline#extensions#ale#enabled = 1
let g:airline_theme = "hybrid"
let g:airline_powerline_fonts = 1

""" Incsearch
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
map <leader>/ :call incsearch#call(incsearch#config#fuzzy#make())<cr>

""" Fugitive
nmap <leader>gs :Gstatus<CR><C-w>20+
nmap <leader>gp :Gpush<CR>

""" Gundo
command! Gundo :GundoToggle

"""" Git
command! GdiffInTab tabedit %|Gdiff
" key `<,D>` -- Git diff in tab
nnoremap <leader>D :GdiffInTab<cr>

"""" CSApprox
let g:CSApprox_attr_map = { 'bold' : 'bold', 'italic' : '', 'sp' : '' }

"""" SnipMate
imap <C-Space> <Plug>snipMateNextOrTrigger
smap <C-Space> <Plug>vnipMateNextOrTrigger

"""" Syntastic
let g:syntastic_enable_signs=1
let g:syntastic_auto_loc_list=2
let g:syntastic_quiet_messages = {'level': 'warnings'}
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }

"""" TagBar
" key `<C-Bs>` -- toggle tagbar
map <silent> <D-Bs> :TagbarToggle<CR>:set noballooneval<CR>

"""" GitGutter
let g:gitgutter_map_keys = 0
let g:gitgutter_sign_added = '∙'
let g:gitgutter_sign_modified = '∙'
let g:gitgutter_sign_removed = '∙'
let g:gitgutter_sign_modified_removed = '∙'

"""" Tex
map <C-S-space> <Plug>IMAP_JumpForward

"""" Powerline
let g:Powerline_stl_path_style = 'short'

"""" Haste
" command `Haste` pastes lines to hastebin
command! -range=% Haste <line1>,<line2>w !haste | tee >(pbcopy)

"""" JSON Prettyfy
" command `Json` prettifies JSON
command! Json %!ruby -rjson -e 'puts JSON.pretty_generate(JSON.parse(STDIN.read))'

""" Test
let test#strategy = "vimterminal"
" let test#strategy = "iterm"

""" Polyglot
let g:polyglot_disabled = ['markdown', 'kotlin']

""" Golang
let g:go_doc_keywordprg_enabled = 0

""" Ack
nnoremap <expr> <leader>f (expand('%') =~ 'NERD_tree' ? "\<c-w>\<c-w>" : '').":Ack!<space>"
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

""" NerdCommenter
let g:NERDSpaceDelims = 1
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1

""" FZF
" set rtp+=/usr/local/opt/fzf
" map <leader>t :Files<CR>

""" CtrlP
let g:ctrlp_map = '<leader>t'
nnoremap <silent> <expr> <leader>t (expand('%') =~ 'NERD_tree' ? "\<c-w>\<c-w>" : '').":CtrlP\<cr>"
nnoremap <silent> <leader>T :CtrlPTag<cr>
" map <space> :CtrlPBuffer<cr>
let g:ctrlp_extensions = ['tag']
if executable('ag')
  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l -g ""'
  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif
" call ctrlp_bdelete#init()

