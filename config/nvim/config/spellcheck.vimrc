""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Spellcheck
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if version >= 700
  set spell spelllang=
  set nospell " Disable spell check by default

  menu Spell.off :setlocal spell spelllang= <cr>
  menu Spell.Russian+English :setlocal spell spelllang=ru,en <cr>
  menu Spell.Russian :setlocal spell spelllang=ru <cr>
  menu Spell.English :setlocal spell spelllang=en <cr>
  menu Spell.-SpellControl- :
  " menu Spell.Word\ Suggest<Tab>z= z=
  " menu Spell.Previous\ Wrong\ Word<Tab>[s [s
  " menu Spell.Next\ Wrong\ Word<Tab>]s ]s
endif


