set termguicolors
" color summerfruit256
" color monokai
" set guifont=Menlo:h18.00
" Colorscheme
" set background=dark
colorscheme gruvbox
autocmd FileType ruby colorscheme railcasts
" autocmd FileType markdown colorscheme mac_classic
" autocmd BufRead,BufNewFile *.md colorscheme hybrid_material

nmap <leader>c1 :colorscheme gruvbox<CR>:set background=dark<CR>
nmap <leader>c2 :colorscheme summerfruit256<CR>
nmap <leader>c3 :colorscheme hybrid_material<CR>
nmap <leader>c4 :colorscheme dracula<CR>:set background=dark<CR>
nmap <leader>c5 :colorscheme spurs-away<CR>:set background=dark<CR>
nmap <leader>c5 :colorscheme pablo<CR>:set background=dark<CR>
nmap <leader>c5 :colorscheme tutticolori<CR><CR>
