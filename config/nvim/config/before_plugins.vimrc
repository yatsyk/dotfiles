let g:mapleader = ','
set nocompatible
filetype off
set encoding=utf-8
set t_Co=256  " make use of 256 terminal colors
let g:snipMate = { 'snippet_version' : 1 }
