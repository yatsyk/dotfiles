filetype plugin indent on  " automatically detect file types

if has("autocmd")
  " In Makefiles, use real tabs, not tabs expanded to spaces
  autocmd FileType make set noexpandtab

  " Make sure all mardown files have the correct filetype set and setup wrapping
  autocmd BufNewFile,BufRead *.{md,markdown,mdown,mkd,mkdn,txt,md.erb} setf markdown
  autocmd BufNewFile,BufRead *.md.erb set ft=markdown

  " Treat JSON files like JavaScript
  autocmd BufNewFile,BufRead *.json set ft=javascript

  autocmd FileType python set softtabstop=4 tabstop=4 shiftwidth=4
  autocmd FileType php set softtabstop=4 tabstop=4 shiftwidth=4
  autocmd FileType java set softtabstop=4 tabstop=4 shiftwidth=4

  " Ruby
  " key `<,r>` -- (ft=ruby) :TestFile
  autocmd FileType ruby nmap <buffer> tn :w<CR>:TestNearest<CR>
  autocmd FileType ruby nmap <buffer> tf :w<CR>:TestFile<CR>
  autocmd FileType ruby nmap <buffer> ts :w<CR>:TestSuite<CR>
  autocmd FileType ruby nmap <buffer> tf :w<CR>:TestFile<CR>
  autocmd FileType ruby nmap <buffer> tl :w<CR>:TestLast<CR>
  autocmd FileType ruby nmap <buffer> tv :w<CR>:TestVisit<CR>

  " Golang
  autocmd FileType go set softtabstop=4 tabstop=4 shiftwidth=4 noexpandtab
  autocmd FileType godoc set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab
  " key `<,R>` -- (ft=go) !go run %
  autocmd FileType go nmap <buffer> <leader>R :w<CR>:!go run %<TAB><CR>
  if has('multi_byte')
    if version >= 700
      autocmd FileType go set listchars=tab:\ \ ,trail:·,extends:❯,precedes:❮,nbsp:×
    endif
  endif

  autocmd FileType markdown setlocal textwidth=79

  " Highlight trailing whitespace
  highlight ExtraWhitespace ctermbg=red guibg=red
  autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
  match ExtraWhitespace /\s\+$/
  autocmd WinEnter * match ExtraWhitespace /\s\+$/
  autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
  autocmd BufWinLeave * call clearmatches()

  " Highlight Gitbook
  autocmd FileType markdown highlight GitbookMethodDefinition guifg=#516185
  autocmd FileType markdown highlight GitbookMethodEnd guifg=#516185
  autocmd FileType markdown highlight GitbookMethodSample guifg=#516185
  autocmd FileType markdown highlight GitbookMethodHeader guibg=#81A1B5 guifg=black gui=bold
  autocmd FileType markdown highlight GitbookMethodHeaderDashes guibg=#81A1B5 guifg=#A1C1D5 gui=bold
  autocmd FileType markdown syntax match GitbookMethodHeaderDashes /#*/ contained
  autocmd FileType markdown syntax match GitbookMethodHeader /#.*/ contained contains=GitbookMethodHeaderDashes
  autocmd FileType markdown syntax region GitbookMethodDefinition start=/^{% method %}/ end=/^#.*$/ keepend contains=GitbookMethodHeader containedin=ALL
  autocmd FileType markdown syntax match GitbookMethodEnd /^{% endmethod %}/ containedin=ALL
  autocmd FileType markdown syntax match GitbookMethodSample /^{% sample [^%]*%}/ containedin=ALL
endif


