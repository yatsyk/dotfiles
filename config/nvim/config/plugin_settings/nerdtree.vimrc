""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" key `<Backspace>` -- toggle NERDTree
" nnoremap <Bs> :NERDTreeToggle<CR>
" key `<S-Backspace>` -- ':NERDTree ' prompt
nnoremap <S-Bs> :NERDTree  "
" key `<,Backspace>` -- :NERDTreeFind find current file in NERDTree
nnoremap <leader><Bs> :NERDTreeFind<CR>

let NERDTreeIgnore=['\.pyc$', '\.pyo$', '\.rbc$', '\.rbo$', '\.class$', '\.o', '\~$']
let NERDTreeHijackNetrw = 0
let NERDTreeDirArrows=1 " Tells the NERD tree to use arrows instead of + ~ chars when displaying directories.

" key `ff` inside NERDTree to find in path
" call NERDTreeAddKeyMap({
      " \ 'key': 'ff',
      " \ 'callback': 'NERDTreeFindInPath',
      " \ 'quickhelpText': 'open in bg tab and close tree',
      " \ 'scope': 'DirNode' })
" function! NERDTreeFindInPath(dirnode)
  " echomsg 'Current node: ' . a:dirnode.path.str()
" endfunction

nnoremap KK :AckInPath! -i "\b<cword>\b"<CR>
command! -bang -nargs=* -complete=file AckInPath call AckInPath('grep<bang>', <q-args>)
function! AckInPath(cmd, args)
  call inputsave()
  let path = input('Search in path: ', expand('%:h').'/', 'dir')
  call inputrestore()
  call ack#Ack(a:cmd, a:args . ' ' . path)
endfunction

if has('autocmd')
  " exit nerdtree if it's the only window
  autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

  augroup AuNERDTreeCmd
  autocmd AuNERDTreeCmd VimEnter * call s:CdIfDirectory(expand("<amatch>"))

  " If the parameter is a directory, cd into it
  function! s:CdIfDirectory(directory)
    let explicitDirectory = isdirectory(a:directory)
    let directory = explicitDirectory || empty(a:directory)

    if explicitDirectory
      exe "cd " . fnameescape(a:directory)
    endif

    " Allows reading from stdin
    " ex: git diff | mvim -R -
    if strlen(a:directory) == 0
      return
    endif

    if directory
      "NERDTree
      "wincmd p
      bd
    endif

    "if explicitDirectory
      "wincmd p
    "endif
  endfunction

  " NERDTree utility function
  function! s:UpdateNERDTree(...)
    let stay = 0

    if(exists("a:1"))
      let stay = a:1
    end

    if exists("t:NERDTreeBufName")
      let nr = bufwinnr(t:NERDTreeBufName)
      if nr != -1
        exe nr . "wincmd w"
        exe substitute(mapcheck("R"), "<CR>", "", "")
        if !stay
          wincmd p
        end
      endif
    endif

    if exists(":CommandTFlush") == 2
      CommandTFlush
    endif
  endfunction
endif

map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark 
map <leader>nf :NERDTreeFind<cr>
map <leader>n :NERDTreeFind<cr>

